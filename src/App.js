import React, { Component } from 'react';
import './App.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {MultiSelect} from 'primereact/multiselect';

const citySelectItems = [
  {label: 'New York', value: 'New York'},
  {label: 'Rome', value: 'Rome'},
  {label: 'London', value: 'London'},
  {label: 'Istanbul', value: 'Istanbul'},
  {label: 'Paris', value: 'Paris'},
  {label: 'Hong Kong', value: 'Hong Kong'},
  {label: 'San Jose', value: 'San Jose'},
  {label: 'San Francisco', value: 'San Francisco'}
];

const placeholder = 'All';

class App extends Component {
  constructor() {
    super();
    this.state = {
      cities: []
    };
    this.tooltip = placeholder;
    this.placeholder = placeholder;
    this.tooltipOptions = {
      position: 'right'
    };
  }
  onchange(e) {
    this.setState({cities: e.value});
    if (e.value.length < 4 && e.value.length > 0) {
      this.placeholder = e.value.join(', ');
      this.tooltip = e.value.join(', ');
    } else if (e.value.length >= 4) {
      this.placeholder = `${e.value.length} items selected`;
      this.tooltip = e.value.join(', ');
    } else {
      this.placeholder = placeholder;
      this.tooltip = placeholder;
    }
  }
  render() {
    return (
      <div className="App">
        <div className="instart">
          <MultiSelect
            placeholder={this.placeholder}
            fixedPlaceholder={true}
            value={this.state.cities}
            options={citySelectItems}
            filter={citySelectItems.length > 7}
            onChange={this.onchange.bind(this)}
            tooltip={this.tooltip}
            tooltipOptions={this.tooltipOptions}
          />
        </div>
      </div>
    );
  }
}

export default App;
